# -*- coding: utf-8 -*-
from distutils.version import StrictVersion
import pip
import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
from pip.req import parse_requirements


class PyTest(TestCommand):

    def __init__(self, *args, **kwargs):
        TestCommand.__init__(self, *args, **kwargs)
        self.test_args = []
        self.test_suite = True

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.test_args)
        sys.exit(errno)


def read_install_requirements():
    # see pip commit d9599ec6 & 5b6d24f1
    if StrictVersion(getattr(pip, '__version__', '1.1')) < StrictVersion('1.2'):
        with open('requirements.txt') as f:
            return f.read().splitlines()
    else:
        return [str(ir.req) for ir in parse_requirements('requirements.txt')]


setup(name='uniencoder',
      version='1.3',
      packages=find_packages(),
      include_package_data=True,
      tests_require=['pytest==2.4.2'],
      install_requires=read_install_requirements(),
      cmdclass={'test': PyTest, },
      url='https://bitbucket.org/eisensheng/uniencoder/',
      license='MIT',
      author='eisensheng',
      author_email='eisensheng@gmail.com',
      description='')
