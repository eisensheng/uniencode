UniEncode
=========

Lossy transliterations of Unicode text into any desired encoding, written
in Python and MIT licensed.

The Idea originates from the Text::Unidecode Perl module by
Sean M. Burke <sburke@cpan.org>.

It often happens that you have text in Unicode or utf-8 encoding, but you need
to represent it in ASCII or any other encoding standard with fewer characters.
This module offers a way to represent the unicode characters which don't fit
into the target encoding in a way which hopefully still allows to grasp the
original idea of the original text. This can't be guaranteed though since the
original character will be replaced with a different (set of) characters that
looks close to it as possible.


Motivation
----------

There are already several Python implementations of the original Perl module
but all of them lack features which can only be found in the other module and
vice versa.

The motivation behind this module was to combine all the good parts and ideas
from the other modules under a more liberal license safe to be used where the
functionality is needed.

I'm aware that there is also an official solution to this problem which
utilizes the ``unicodedata`` module. The official solution can't handle
overrides in the mapping, supports only normalization to ascii and is limited
in supported characters though.


Features
--------

* Dynamically transliterate, convert and normalize unicode text into ascii
  encoding or any other desired encoding.
* Use different translation mappings from any kind of source 
  (Files, entire Directories or other Streams).
* Rich mapping definition syntax


Installation
------------

To install uniencode from the source root simply run

.. code-block:: bash

    $ python setup.py install

or install it directly with pip

.. code-block:: bash

    $ pip install git+ssh://git@bitbucket.org/eisensheng/uniencoder.git#egg=uniencoder


Requirements
------------

This module depends on a few extra tools which can be easily installed from
the pypi. These will be automatically installed if this package has been
installed by pip.


Examples
--------

Uniencode ships with a default mapping useful enough for every day work and
ready to be used out of the box.

.. code-block:: python

    from uniencoder import uniencode

    print uniencode(u'Max Müller')


Testing
-------

Tests are handled by tox and pytest. Testing can simply be done by running

.. code-block:: bash

    $ python setup.py test


Python3
-------
Unfortunately this module is not compatible with python3 due to an issue with
the shlex module. The module is responsible for mapping file parsing.
Shlex behaves inconsistently between Python2 and Python3 with unicode data.
Therefore only Python2.6, Python2.7 and PyPy are enabled in the tox
testing grid.
