# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals
from uniencoder.uniencoder import UniEncoder
from pkg_resources import resource_filename

__version__ = '1.0'

_encoder = UniEncoder()
_encoder.reader.load_mapping(resource_filename(__name__, 'mapping'))


def uniencode(*args, **kwargs):
    return _encoder.encode(*args, **kwargs)
