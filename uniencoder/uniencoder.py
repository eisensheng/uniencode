# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals
import codecs
import io
from shlex import split
import sys

from path import path as p


class GrowingMapping(object):

    def __init__(self):
        self.data = []  # use a list for performance reasons

    def __getitem__(self, item):
        value = self.data[item]
        if value is None:
            raise IndexError('index not assigned')
        return self.data[item]

    def __setitem__(self, key, value):
        if key > sys.maxunicode:
            raise ValueError('value out of range')

        for retry in range(2):
            try:
                self.data[key] = value
            except IndexError:
                # extend the data array up to the requested index
                self.data.extend([None, ] * (key - len(self.data) + 1))
                continue  # retry
            else:
                break
        else:
            raise IndexError('assignment index out of range')


class MappingReader(object):

    def __init__(self, mapping):
        self.mapping = mapping
        self.position = 0

    def _insert_mapping(self, char):
        self.mapping[self.position] = char.encode('ascii')
        self.position += 1

    def _instruction_seek(self, position):
        self.position = int(position)

    def _instruction_page(self, page):
        self.position = int(page) * 256

    def _instruction_declare(self, *args):
        for x in args:
            self._insert_mapping(x)

    def _instruction_repeat(self, times, value):
        for x in range(int(times)):
            self._insert_mapping(value)

    def load_mapping_from_stream(self, source):
        self.position = 0
        for i, line in enumerate(source, start=1):
            try:
                # unfortunately, the shlex implementation can’t handle UTF-8
                # properly.
                args = map(lambda s: s.decode('utf-8'),
                           split(line.encode('utf-8'), True))
            except ValueError as exc:
                raise ValueError('{0}:{1} {2!r}: {3!s}'.format(source, i,
                                                               line, exc))
            if not args:  # ignore empty lines
                continue

            value = args[0]
            if value.startswith('.'):  # it's actually an instruction
                try:
                    caller = getattr(self, '_instruction_' + value[1:])
                except AttributeError:
                    raise ValueError('Bad Command: {0!r}'.format(value))
                else:
                    caller(*args[1:])
            else:
                self._insert_mapping(value)

    def load_mapping_from_file(self, source):
        with io.open(source) as stream:
            self.load_mapping_from_stream(stream)

    def load_mapping_from_dir(self, source):
        source_path = p(source)
        for x in source_path.listdir():
            self.load_mapping(x)

    def load_mapping(self, source):
        if hasattr(source, 'read'):  # appears to be some sort of a stream
            self.load_mapping_from_stream(source)
        else:
            source_path = p(source)
            if source_path.isdir():
                self.load_mapping_from_dir(source)
            else:
                self.load_mapping_from_file(source)


class UniEncoder(object):

    def __init__(self):
        self.mapping = GrowingMapping()

    @property
    def reader(self):
        return MappingReader(self.mapping)

    def encode(self, text, encoding='ascii'):
        codec = codecs.lookup(encoding)

        encoded_chars = []
        for unicode_char in text:
            try:
                chunk = codec.encode(unicode_char)[0]
            except UnicodeEncodeError:
                code_point = ord(unicode_char)
                try:
                    chunk = self.mapping[code_point]
                except IndexError:
                    chunk = '?'
            encoded_chars += chunk,
        return b''.join(encoded_chars)
