# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals
import pytest
import six
from io import StringIO
from path import path as p
from pkg_resources import resource_filename
from uniencoder import uniencode, UniEncoder
from uniencoder.uniencoder import GrowingMapping, MappingReader


def _get_fixture_file_path(file_name):
    return resource_filename(__name__, p('data') / file_name)


@pytest.fixture
def mapping():
    return GrowingMapping()


def test_mapping_growing(mapping):
    mapping[52] = 42
    assert mapping[52] == 42


def test_mapping_empty_point(mapping):
    mapping[52] = 37
    with pytest.raises(IndexError):
        assert not isinstance(mapping[42], None)


@pytest.fixture
def reader():
    m = GrowingMapping()
    r = MappingReader(m)
    return r


def test_reader_simple_mapping(reader):
    reader.load_mapping(_get_fixture_file_path('sample_mapping'))
    assert reader.mapping[42] == 'a'


def test_reader_from_stream(reader):
    stream = StringIO('''
            .seek 42
            a
                b
    ''')
    reader.load_mapping(stream)
    assert reader.mapping[42] == 'a'
    assert reader.mapping[43] == 'b'


def test_reader_bad_mapping(reader):
    with pytest.raises(ValueError):
        reader.load_mapping(_get_fixture_file_path('bad_mapping'))


def test_reader_multi_declare(reader):
    reader.load_mapping(_get_fixture_file_path('multi_declare'))
    data = reader.mapping.data

    assert len(data) == 25606

    for x in range(25600):
        assert data[x] is None

    assert data[25600] == 'a'
    assert data[25601] == 'b'
    assert data[25602] == 'c'
    assert data[25603] == 'd'
    assert data[25604] == 'e'
    assert data[25605] == ' f '


def test_reader_repeat(reader):
    reader.load_mapping(_get_fixture_file_path('repeating_mapping'))
    data = reader.mapping.data

    assert len(data) == 2568
    for x in range(2560):
        assert data[x] is None

    assert data[2560:] == ['[?]', ] * 8


def test_reader_bad_command(reader):
    with pytest.raises(ValueError):
        reader.load_mapping(_get_fixture_file_path('bad_command'))


@pytest.mark.parametrize(("text", "result"),
                         [("Programmes de publicité - Solutions d'entreprise",
                           b"Programmes de publicite - Solutions d'entreprise"),
                          ("Транслитерирует и русский",
                           b"Translitieriruiet i russkii"),
                          ("kožušček", b"kozuscek"),
                          ("北亰", b"Bei Jing "), ])
def test_transliterate(text, result):
    out = uniencode(text)
    assert isinstance(out, six.binary_type)
    assert out == result


def test_encoded():
    assert (repr(uniencode('Jordăn Müller', '850')) == br"'Jordan M\x81ller'")
